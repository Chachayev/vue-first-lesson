// Components
import MyButton from "./my-button.vue";
import MyInput from "./my-input.vue";
import MyModal from "./my-modal.vue";
// Styles
import "@/styles/my-button.scss";
import "@/styles/my-input.scss";
import "@/styles/my-modal.scss";

export default [
    MyButton, MyInput, MyModal
]